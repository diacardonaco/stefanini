package com.stefanini.stefanini.controller;

import com.stefanini.stefanini.entity.Estado;
import com.stefanini.stefanini.entity.Persona;
import com.stefanini.stefanini.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/estado")
public class EstadoController {



        @Autowired
        EstadoService estadoService;

        @GetMapping("/estados")
        public ResponseEntity<List<Estado>> list(){
            List<Estado> list  = estadoService.list();
            return  new ResponseEntity<List<Estado>>(list, HttpStatus.OK);
        }

        @GetMapping("/estadoporcodigo/{codigo}")
        public ResponseEntity<Persona> getById(@PathVariable("codigo") int codigo){
            List<Estado> list  = estadoService.list();
            if(!estadoService.existsById(codigo)){
                return  new ResponseEntity("no existe", HttpStatus.OK);
            }
            Optional<Estado> estado = estadoService.getOne(codigo);
            return  new ResponseEntity(estado, HttpStatus.OK);
        }


        @PostMapping(value = "/guardarEstado")
        public ResponseEntity<Estado> guardarEstado(@RequestBody Estado estado){
            estadoService.save(estado);
            return  new ResponseEntity(estado, HttpStatus.OK);
        }


        @DeleteMapping(value = "/eliminarEstado/{codigo}")
        public ResponseEntity<Estado> eliminarEstado(@PathVariable("codigo") int codigo){
            estadoService.delete(codigo);
            return  new ResponseEntity("estado eliminado", HttpStatus.OK);
        }


        @PutMapping(value = "/actualizarEstado")
        public ResponseEntity<Estado> actualizarEstado(@RequestBody Estado estado){
            estadoService.save(estado);
            return  new ResponseEntity(estado, HttpStatus.OK);
        }













    }
