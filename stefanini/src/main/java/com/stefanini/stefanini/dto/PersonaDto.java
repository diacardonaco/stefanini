package com.stefanini.stefanini.dto;

import java.util.Date;

public class PersonaDto {

    private int codigo;
    private String nombre;
    private String apellido;
    private Date fecha_nacimiento;
    private String username;
    private String password;
    private int identificacion;
    private int codigo_tipo_identificacion;
    private int codigo_estado;

    public PersonaDto() {
    }

    public PersonaDto(int codigo, String nombre, String apellido, Date fecha_nacimiento, String username, String password, int identificacion, int codigo_tipo_identificacion, int codigo_estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fecha_nacimiento = fecha_nacimiento;
        this.username = username;
        this.password = password;
        this.identificacion = identificacion;
        this.codigo_tipo_identificacion = codigo_tipo_identificacion;
        this.codigo_estado = codigo_estado;
    }
}
