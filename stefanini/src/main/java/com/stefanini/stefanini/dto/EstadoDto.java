package com.stefanini.stefanini.dto;

import java.util.Date;

public class EstadoDto {
    private int codigo;
    private String nombre;
    private Date fecha_creacion;
    private String usuario_creacion;
    private Date fecha_modificacion;
    private String usuario_modificacion;
}
