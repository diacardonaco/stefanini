package com.stefanini.stefanini.repository;

import com.stefanini.stefanini.entity.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EstadoRepository  extends JpaRepository<Estado, Integer> {

    Optional<Estado> findByNombre(String nombre);
    boolean existsByNombre(String nombre);

}
