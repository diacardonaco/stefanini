package com.stefanini.stefanini.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;
    private String nombre;
    private String apellido;
    private Date fecha_nacimiento;
    private String username;
    private String password;
    private int identificacion;
    private int codigo_tipo_identificacion;
    private int codigo_estado;

    public Persona() {
    }

    public Persona(int codigo, String nombre, String apellido, Date fecha_nacimiento, String username, String password, int identificacion, int codigo_tipo_identificacion, int codigo_estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fecha_nacimiento = fecha_nacimiento;
        this.username = username;
        this.password = password;
        this.identificacion = identificacion;
        this.codigo_tipo_identificacion = codigo_tipo_identificacion;
        this.codigo_estado = codigo_estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public int getCodigo_tipo_identificacion() {
        return codigo_tipo_identificacion;
    }

    public void setCodigo_tipo_identificacion(int codigo_tipo_identificacion) {
        this.codigo_tipo_identificacion = codigo_tipo_identificacion;
    }

    public int getCodigo_estado() {
        return codigo_estado;
    }

    public void setCodigo_estado(int codigo_estado) {
        this.codigo_estado = codigo_estado;
    }
}
